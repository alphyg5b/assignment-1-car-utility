package com.carutility.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import com.carutility.entities.Car;
import com.carutility.entities.Customer;
import com.carutility.util.Enums;


public class AdminOps{
	
	private static ArrayList<Customer> custs;
	static Map<Integer,Customer> m = new HashMap<Integer,Customer>();
	
	
	
	public static void main(String[] args){
		custs = new ArrayList<Customer>();
		System.out.println("enter choice");
		System.out.println("ACUST to add customer");
		System.out.println("ACAR to add car");
		System.out.println("LISCUS to list customer with id");
		System.out.println("LISALL to list all customers");
		System.out.println("PRZ to create rewards");
		Scanner sc = new Scanner(System.in);
		do{
			Enums.Choices t = Enums.Choices.valueOf(sc.nextLine());
			String p= null;
			String[] s = null;
			int id;
			switch(t){
			case ACUST:
				System.out.println("id<space>name");
				p = sc.nextLine();
				s = p.split(" ");
				addCust(s);
				break;
			case ACAR:
				System.out.println("customer id");
				id = Integer.parseInt(sc.nextLine());
				System.out.println("car details - id model type price");
				p = sc.nextLine();
				s = p.split(" ");
				addCar(id,s);
				break;
			case LISCUS:
				System.out.println("enter customer id");
				id = Integer.parseInt(sc.nextLine());
				listCust(id);
				break;
			case LISALL:
				listAll();
				break;
			case PRZ:
				prizes();
				break;
			}
			System.out.println("more??");
		}while(!sc.nextLine().equals("n"));
	}
	
	public static void addCust(String s[]){
		Customer c = new Customer(s);
		m.put(c.getId(), c);
		custs.add(c);
	}
	
	public static void addCar(int id, String s[]){
		Customer c = m.get(id);
		ArrayList<Car> list = c.getCars();
		Car car = new Car(s);
		list.add(car);
	}
	
	public static void listCust(int id){
		Customer c = m.get(id);
		System.out.println(c.getName());
	}
	
	public static void listAll(){
		//Customer a[] = null;
		ArrayList<Customer> nlist = new ArrayList<Customer>(m.values());
		
		Collections.sort(nlist, new Comparator<Customer>() {
	        @Override
	        public int compare(Customer c1, Customer c2){
				return c1.getName().compareTo(c2.getName());
			}
	    });
		for(Customer i:nlist){
			System.out.println(i.getId() + i.getName());
		}
	}
	
	public static void prizes(){
		Set<Integer> s = m.keySet();
		Random r = new Random();
		ArrayList<Integer> a = new ArrayList<Integer>();
		for(int i=0;i<5;i++){
			int k = r.nextInt(s.size());
			a.add(k);
			s.remove(a.get(i));
		}
		Scanner scan = new Scanner(System.in);
		int[] b = new int[3];
		System.out.println("3 ids - 1 in each line");
		for(int i=0;i<3;i++){
			b[i] = Integer.parseInt(scan.nextLine());
			if(a.contains(b[i])){
				System.out.println(b[i]);
			}
		}
		
	}
}

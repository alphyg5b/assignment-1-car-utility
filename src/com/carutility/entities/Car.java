package com.carutility.entities;

import com.carutility.util.Enums;
import com.carutility.util.Enums.Type;

public class Car {
	private int id;
	private String model;
	private Type type;
	private int price;
	private float rPrice;
	
	
	
	public Car(String s[]){
		this.id = Integer.parseInt(s[0]);
		this.model = s[1];
		switch(Integer.parseInt(s[2])){
		case 1:
			this.type = Type.TOYOTA;
		case 2:
			this.type = Type.MARUTI;
		case 3:
			this.type = Type.HYUNDAI;
		}
		this.price = Integer.parseInt(s[3]);
		switch(type){
		case TOYOTA:
			this.rPrice = (float) (0.8*this.price);
		case MARUTI:
			this.rPrice = (float) (0.6*this.price);
		case HYUNDAI:
			this.rPrice = (float) (0.4*this.price);
		}
	}
}

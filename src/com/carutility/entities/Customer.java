package com.carutility.entities;

import java.util.ArrayList;

public class Customer{
	private int id;
	private String name;
	private ArrayList<Car> list;
	//Map<Integer,Customer> m = new HashMap<Integer,Customer>();
	
	public Customer(String s[]){
		this.id = Integer.parseInt(s[0]);
		this.name = s[1];
		list = new ArrayList<Car>();
	}
	
	public int getId(){
		return this.id;
	}
	
	public ArrayList<Car> getCars(){
		return this.list;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String toString(Customer c){
		return c.id + " " + c.name;
	}

}
